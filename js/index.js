import { Showcase } from "./Showcase";
import { Slides } from "./Slides";
import { Cursor } from "./Cursor";
import image1 from '/images/1.jpg';
import image2 from '/images/2.jpg';
import image3 from '/images/3.jpg';
import image4 from '/images/4.jpg';
import image5 from '/images/5.jpg';

// Audios 

import audio1 from "/audio/1Loved-You-REPLAY-STREAMING-5-(1).mp3";
import audio2 from "/audio/2-Not-Us-Anymore-STREAMING-MIX-1.mp3";
import audio3 from "/audio/3-Chase-Me-Down-STREAM-VERSION-1.mp3";
import audio4 from "/audio/4-Myself-REPLAY-STREAMING-1-(1).mp3";
import audio5 from "/audio/5-Let-You-Go-STREAMING-MIX-1-(1).mp3";
import audio6 from "/audio/6-Faithful-REPLAY-STREAMING-1-(2).mp3";
import audio7 from "/audio/7-Ghost-REPLAY-STREAMING-1-(1).mp3";
import audio8 from "/audio/8-Leave-You-STREAMING-MIX-1.mp3";
import audio9 from "/audio/9-Fall-Out-STREAMING-MIX-VERSION-1-(1).mp3";

const container = document.getElementById("app");
const cursor = new Cursor(document.querySelector('.cursor'));
const slidesData = [
  {
    number:"01",
    audio: audio1,
    autoplay: false,
    image: image1,
    title: "Loved you",
    meta: "inbetween"
  },
  {
    number:"02",
    image: image2,
    audio: audio2,
    autoplay: false,
    title: "not us anymore",
    meta: "inbetween"
  },
  {
    number:"03",
    image: image3,
    audio: audio3,
    autoplay: true,
    title: "Chase",
    meta: "inbetween"
  },
  {
    number:"04",
    image: image4,
    audio: audio4,
    autoplay: false,
    title: "myself",
    meta: "inbetween"
  },
  {
    number:"05",
    image: image5,
    audio: audio5,
    autoplay: false,
    title: "let you go",
    meta: "inbetween"
  }
];

const slides = new Slides(slidesData);
const showcase = new Showcase(slidesData, {
  onActiveIndexChange: activeIndex => {
    slides.onActiveIndexChange(activeIndex);
  },
  onIndexChange: index => {
    slides.onMove(index);
  },
  onZoomOutStart: ({ activeIndex }) => {
    cursor.enter();
    slides.appear();
  },
  onZoomOutFinish: ({ activeIndex }) => {
  },
  onFullscreenStart: ({ activeIndex }) => {
    cursor.leave();
    slides.disperse(activeIndex);
  },
  onFullscreenFinish: ({ activeIndex }) => {
  }
});

showcase.mount(container);
slides.mount(container);
showcase.render();

window.addEventListener("resize", function() {
  showcase.onResize();
});

window.addEventListener("mousemove", function(ev) {
  showcase.onMouseMove(ev);
});