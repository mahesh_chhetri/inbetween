const createEleWithClass = (tag, className) => {
  const ele = document.createElement(tag);
  ele.className = className;
  return ele;
};


class Slides {
  constructor(data) {
    this.data = data;
    this.container = createEleWithClass("div", "slides");
    this.currentIdx = 0;
    this.slides = this.data.map((entry, index) => {
      const slide = createEleWithClass("div", "slide");
      const title = createEleWithClass("h1", "slide-title");
      const meta = createEleWithClass("p", "slide-meta");
      // const more = createEleWithClass("a", "slide-more");
      const icon = createEleWithClass("i", "fa fa-volume-up fa-4x");
      const playTestButon= createEleWithClass("button",'btn btn-danger');
      playTestButon.setAttribute('id', 'play-btn');
      playTestButon.innerHTML = "Click to play";
      //
      // more.href = "#"; 
      slide.classList.add(index !== 0 ? "next" : "show-meta");
      meta.innerHTML = entry.meta;
      title.innerHTML = entry.title;
      // more.innerHTML = 'Read more';
      slide.appendChild(title);
      slide.appendChild(meta);
      // slide.appendChild(more);
      slide.appendChild(icon);
      slide.appendChild(playTestButon);
      icon.addEventListener('click', this.onClickPlay);
      // executing audio config
      var audio = this.audioConfig(entry, index);
      slide.appendChild(audio);
      this.container.appendChild(slide);
      console.log("Slide INIT");
      return slide;
    });
  }

  // Creating audio element and setting al the required attributes
  audioConfig(entry, index) {
    const audio = createEleWithClass("audio", "test");
    audio.setAttribute("src", entry.audio);
    if (entry.autoplay) {
      audio.setAttribute("autoplay", entry.autoplay);
      audio.setAttribute("isPlaying", true);
      audio.setAttribute("autostart", 1);
    }
    audio.setAttribute("id", "audio-" + index);
    return audio;
  }

  //  click event to icon to play
  onClickPlay(event) {
    var audios = document.querySelectorAll('audio');
    audios.forEach(function (audio) {
      audio.setAttribute("isPlaying", false);
      audio.pause();
    });
    event.target.nextElementSibling.setAttribute('isPlaying', true);
    event.target.nextElementSibling.play();
  }




  mount(container) {
    container.appendChild(this.container);
  }
  onActiveIndexChange(activeIndex) {
    this.currentIdx = activeIndex;
    for (let i = 0; i < this.slides.length; i++) {
      if (activeIndex === i) {
        this.slides[i].classList.remove("next");
        this.slides[i].classList.remove("prev");
      } else {
        if (activeIndex > i) {
          this.slides[i].classList.remove("next");
          this.slides[i].classList.add("prev");
        } else {
          this.slides[i].classList.add("next");
          this.slides[i].classList.remove("prev");
        }
      }
    }
    this.audioPlayOnSlideChange(activeIndex);
  }
  onMove(indexFloat) {
    this.container.style.transform = `translateY(${(indexFloat * 100) /
      this.slides.length}%)`;
  }

  // audio control on moving slide
  audioPlayOnSlideChange(slideIndex) {
    var audios = document.querySelectorAll('audio');
    audios.forEach(function (audio) {
      audio.setAttribute("isPlaying", false);
      audio.pause();
    });
    var currentSlideAudio = this.slides[slideIndex].querySelector('audio');
    currentSlideAudio.setAttribute('isPlaying', true);
    currentSlideAudio.play();
    this.soundControlOnDragAndRelease(0.2);
  }

  appear() {
    this.container.classList.add("scrolling");
    this.slides[this.currentIdx].classList.remove("show-meta");
    this.soundControlOnDragAndRelease(0.2);
  }
  disperse(activeIndex) {
    //this.currentIdx = activeIndex;
    this.slides[this.currentIdx].classList.add("show-meta");
    this.container.classList.remove("scrolling");
    for (let index = 0; index < this.data.length; index++) {
      if (index > activeIndex) {
        this.slides[index].classList.add("next");
        this.slides[index].classList.remove("prev");
      } else if (index < activeIndex) {
        this.slides[index].classList.remove("next");
        this.slides[index].classList.add("prev");
      } else {
        this.slides[index].classList.remove("next");
        this.slides[index].classList.remove("prev");
      }
    }
    // Calling sound func
    this.soundControlOnDragAndRelease(1);
  }

  soundControlOnDragAndRelease(volume) {
    // resetting volume
    var currentPlayingAudio = document.querySelector('audio[isPlaying="true"]');
    if (currentPlayingAudio) {
      currentPlayingAudio.volume = volume;
    }
  }

}

export { Slides };
